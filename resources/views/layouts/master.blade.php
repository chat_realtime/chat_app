<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Document</title>
    @vite(['resources/scss/avatar.scss','resources/js/app.js'])
</head>
<body>
<div class="container-fluid ">
    <div class="row ">
        <div class="navigation navbar navbar-light bg-primary">
{{--            <x-aside_left></x-aside_left>--}}
        </div>

        <aside class="sidebar">
{{--            {{$slot}}--}}
        </aside>

        <main class="main main-visible">
{{--                {{$slot}}--}}
        </main>

        <div class="appbar">
{{--            <x-aside_right></x-aside_right>--}}
        </div>

    </div>
</div>

@if (isset($script))
    {{$script}}
@endif

</body>
</html>
