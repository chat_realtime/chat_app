<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Conversation extends Model
{
    use HasFactory;
    protected $primaryKey = 'conversation_id';

    protected $fillable = [
        'name',
    ];

    public function groupMembers()
    {
        return $this->hasMany(GroupMember::class, 'conversation_id');
    }
}
