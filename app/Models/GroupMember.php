<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class GroupMember extends Model
{
    use HasFactory;
    protected $primaryKey = 'group_member_id';

    protected $fillable = [
        'conversation_id',
        'contact_user_id',
        'joined_datetime',
    ];

    public function conversation()
    {
        return $this->belongsTo(Conversation::class, 'conversation_id');
    }

    public function contact()
    {
        return $this->belongsTo(Contact::class, 'contact_user_id');
    }
}
