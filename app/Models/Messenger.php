<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Messenger extends Model
{
    use HasFactory;

    protected $table = 'messenger';
    protected $primaryKey = 'messenger_id';

    protected $fillable = [
        'user_id',
        'to_user_id',
        'messenger_text',
        'sent_datetime',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
