<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class User extends Model
{
    use HasFactory;
    protected $primaryKey = 'user_id';

    protected $fillable = [
        'name',
        'password',
        'avatar',
        'role',
        'status',
        'email',
        'address',
        'created_at',
    ];

    public function contacts()
    {
        return $this->hasMany(Contact::class, 'user_id');
    }

    public function messenger()
    {
        return $this->hasMany(Messenger::class, 'user_id');
    }
}
