<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Contact extends Model
{
    use HasFactory;
    protected $primaryKey = 'contact_id';

    protected $fillable = [
        'user_id',
        'contact_user_id',
        'first_name',
        'last_name',
        'avatar',
        'phone_number',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
