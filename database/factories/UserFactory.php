<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    protected $model = User::class;

    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'password' => Hash::make('password'),
            'avatar' => $this->faker->imageUrl(),
            'role' => $this->faker->randomElement(['admin', 'user']),
            'status' => $this->faker->randomElement(['online', 'offline', 'busy']),
            'email' => $this->faker->unique()->safeEmail,
            'address' => $this->faker->address,
            'created_at' => $this->faker->dateTimeBetween('-1 year', 'now'),
        ];
    }
}
