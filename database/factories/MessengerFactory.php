<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Faker\Generator as Faker;
use App\Models\Messenger;
use App\Models\User;

class MessengerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Messenger::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::factory(),
            'to_user_id' => User::factory(),
            'messenger_text' => $this->faker->sentence,
            'sent_datetime' => $this->faker->dateTimeBetween('-1 year', 'now'),
        ];
    }
}
