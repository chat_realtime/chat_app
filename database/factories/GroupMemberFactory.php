<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Generator as Faker;
use App\Models\GroupMember;
use App\Models\Conversation;
use App\Models\Contact;

class GroupMemberFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = GroupMember::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'conversation_id' => Conversation::factory(),
            'contact_user_id' => Contact::factory(),
            'joined_datetime' => $this->faker->dateTimeBetween('-1 year', 'now'),
        ];
    }
}
