<?php

namespace Database\Seeders;

use App\Models\Contact;
use App\Models\Conversation;
use App\Models\GroupMember;
use App\Models\Messenger;
use App\Models\User;
use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        User::factory(100)->create();
        Contact::factory(100)->create();
        Conversation::factory(100)->create();
        Messenger::factory(100)->create();
        GroupMember::factory(100)->create();
    }
}
